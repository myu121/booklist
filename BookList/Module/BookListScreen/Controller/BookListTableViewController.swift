//
//  BookListViewController.swift
//  BookList
//
//  Created by Michael Yu on 3/24/21.
//

import Foundation
import UIKit

class BookListViewController: UITableViewController {
    private var books: [Book] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Book List"
        self.populateDataFromJson()
        print(books[0])
        // Do any additional setup after loading the view.
    }
    
    private func populateDataFromJson() {
        if let path = Bundle.main.path(forResource: "books", ofType: "json") {
            do {
                let dataJson = try Data(contentsOf: URL(fileURLWithPath: path))
                let jsonDict = try JSONSerialization.jsonObject(with: dataJson, options: .mutableContainers)
                if let jsonResults = jsonDict as? [[String: Any]] {
                    jsonResults.forEach { bookDict in
                        if let volumeInfo = bookDict["volumeInfo"] as? [String: Any],
                           let imageLinks = volumeInfo["imageLinks"] as? [String: Any],
                           let saleInfo = bookDict["saleInfo"] as? [String: Any],
                           let listPrice = saleInfo["listPrice"] as? [String: Any] {
                            books.append(Book(
                                title: volumeInfo["title"] as? String,
                                authors: volumeInfo["authors"] as? [String],
                                id: bookDict["id"] as? String,
                                publisher: volumeInfo["publisher"] as? String,
                                publishedDate: volumeInfo["publishedDate"] as? String,
                                description: volumeInfo["description"] as? String,
                                pageCount: volumeInfo["pageCount"] as? Int,
                                categories: volumeInfo["categories"] as? [String],
                                averageRating: volumeInfo["averageRating"] as? Double,
                                smallThumbnail: imageLinks["smallThumbnail"] as? String,
                                thumbnail: imageLinks["thumbnail"] as? String,
                                previewLink: volumeInfo["previewLink"] as? String,
                                isEbook: saleInfo["isEbook"] as? Bool,
                                price: listPrice["amount"] as? Double,
                                buyLink: saleInfo["buyLink"] as? String))
                        }
                        
                    }
                }
            } catch {
                print("No json data found")
            }
        }
    }
}
